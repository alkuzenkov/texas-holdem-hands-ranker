<?php

use function DI\autowire;

return [
    /** Kernel */
    \Psr\Container\ContainerInterface::class                    => function () {
        return \Ranker\App::i()->getContainer();
    },

    /** Controllers */
    \Ranker\Contracts\Controllers\RankerController::class       => autowire(\Ranker\Controllers\RankerController::class),

    /** Services */
    \Ranker\Contracts\Services\Card\SuitService::class          => autowire(\Ranker\Services\Card\SuitService::class),
    \Ranker\Contracts\Services\Card\ValueService::class         => autowire(\Ranker\Services\Card\ValueService::class),
    \Ranker\Contracts\Services\CardService::class               => autowire(\Ranker\Services\CardService::class),
    \Ranker\Contracts\Services\HandService::class               => autowire(\Ranker\Services\HandService::class),

    /** Repositories */
    \Ranker\Contracts\Repositories\Card\SuitRepository::class   => autowire(\Ranker\Repositories\Card\SuitRepository::class),
    \Ranker\Contracts\Repositories\Card\ValueRepository::class  => autowire(\Ranker\Repositories\Card\ValueRepository::class),
    \Ranker\Contracts\Repositories\CombinationRepository::class => autowire(\Ranker\Repositories\CombinationRepository::class),

    /** Models */
    \Ranker\Contracts\Models\Hand::class                        => \Ranker\Models\Hand::class,
    \Ranker\Contracts\Models\Card::class                        => \Ranker\Models\Card::class,
];
