<?php

namespace Ranker\Strategies\Input;

use Ranker\Contracts\Strategies\InputStrategy;
use Ranker\Exception\BadInputException;

/**
 * Class FileStrategy
 * @package Ranker\Strategies\Input
 */
class FileStrategy implements InputStrategy {

    protected const HANDS_DELIMITER = PHP_EOL;
    protected const CARDS_DELIMITER = ' ';

    /**
     * @param mixed $data
     * @return array
     */
    public function parseData($data): array {
        if (!is_string($data)) {
            throw new BadInputException('Variable must be a path to the input file');
        }
        $content = file_get_contents($data);
        if (!$content) {
            throw new BadInputException('Empty file content of ' . $data);
        }
        $result = [];
        foreach (explode(self::HANDS_DELIMITER, $content) as $hand) {
            $hand = trim($hand);
            if (!$hand) {
                break;
            }
            $result[] = explode(self::CARDS_DELIMITER, $hand);
        }
        return $result;
    }
}