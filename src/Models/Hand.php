<?php

namespace Ranker\Models;

use Ranker\Contracts\Models\Card;
use Ranker\Contracts\Models\Hand as ModelContract;
use Ranker\Exception\Hand\CardsCountException;

class Hand implements ModelContract {

    /** @var int */
    protected $id;

    /** @var Card[] */
    protected $cards = [];

    /**
     * @param int $id
     */
    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    /**
     * @param Card $card
     */
    public function addCard(Card $card): void {
        if ($this->getCardsCount() > 5) {
            throw new CardsCountException('Too much cards in hand');
        }
        $this->cards[] = $card;
    }

    /**
     * @return int
     */
    public function getCardsCount(): int {
        return count($this->getCards());
    }

    /**
     * @return Card[]
     */
    public function getCards(): array {
        return $this->cards;
    }

    /**
     * @return string
     */
    public function __toString() {
        $result = '';
        foreach ($this->getCards() as $card) {
            $result .= (string)$card . ' ';
        }
        return trim($result);
    }

}
