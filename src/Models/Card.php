<?php

namespace Ranker\Models;

use Ranker\Contracts\Models\Card as ModelContract;

/**
 * Class Card
 * @package Ranker\Models
 */
class Card implements ModelContract {

    /** @var string */
    protected $value;

    /** @var string */
    protected $suit;

    /**
     * @return string
     */
    public function getValue(): string {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getSuit(): string {
        return $this->suit;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value): void {
        $this->value = $value;
    }

    /**
     * @param string $suit
     */
    public function setSuit(string $suit): void {
        $this->suit = $suit;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->value . $this->suit;
    }

}
