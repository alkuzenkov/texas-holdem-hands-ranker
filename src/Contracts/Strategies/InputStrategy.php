<?php

namespace Ranker\Contracts\Strategies;

/**
 * Interface InputStrategy
 * @package Ranker\Contracts\Strategies
 */
interface InputStrategy {

    /**
     * Parse input data
     * @param mixed $data
     * @return array
     */
    public function parseData($data): array;

}