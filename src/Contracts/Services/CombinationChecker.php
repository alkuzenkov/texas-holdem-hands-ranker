<?php

namespace Ranker\Contracts\Services;

use Ranker\Contracts\Models\Hand;

/**
 * Interface CombinationChecker
 * @package Ranker\Contracts\Services
 */
interface CombinationChecker {

    /**
     * Check combination of cards in one hand by rule
     * @param Hand $hand
     * @return bool
     */
    public function matches(Hand $hand): bool;

}