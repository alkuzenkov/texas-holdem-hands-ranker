<?php

namespace Ranker\Contracts\Services;

use Ranker\Contracts\Models\Card;

/**
 * Interface CardService
 * @package Ranker\Contracts\Services
 */
interface CardService {

    /**
     * @param string $card
     * @return Card
     */
    public function createCardFromString(string $card): Card;

    /**
     * Sort cards by rank (ASC)
     * @param Card[] $cards
     * @return Card[]
     */
    public function sort(array $cards): array;

    /**
     * Compare card values like in spaceship operator (<=>).
     * @param Card $a
     * @param Card $b
     * @return int
     */
    public function compare(Card $a, Card $b): int;

    /**
     * Checks if values of two cards are close to each other. E.g. 9 and 10 are close while 9 and J aren't.
     * @param Card $a
     * @param Card $b
     * @return bool
     */
    public function isCloseValues(Card $a, Card $b): bool;

}
