<?php

namespace Ranker\Contracts\Services;

use Ranker\Contracts\Models\Hand;

/**
 * Interface HandService
 * @package Ranker\Contracts\Services
 */
interface HandService {

    /**
     * Creates hand from array like ["4♠", "J♠", "8♠", "2♠", "9♠"]
     * @param int   $id
     * @param array $cards
     * @return Hand
     */
    public function createHandFromArrayOfStrings(int $id, array $cards): Hand;

    /**
     * Sort hands by cards combination ranks
     * @param Hand[] $hands
     * @return Hand[]
     */
    public function rank(array $hands): array;

}
