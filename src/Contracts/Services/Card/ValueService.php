<?php

namespace Ranker\Contracts\Services\Card;

/**
 * Operations with card values
 * @package Ranker\Contracts\Services\Card
 */
interface ValueService {
    /**
     * Extracts card value from string like "10♦"
     * @param string $card
     * @return string
     */
    public function getValueFromString(string $card): string;

}
