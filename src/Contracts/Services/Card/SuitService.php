<?php

namespace Ranker\Contracts\Services\Card;

/**
 * Operations with suits
 * @package Ranker\Contracts\Services\Card
 */
interface SuitService {

    /**
     * Extracts suit from string like "10♦"
     * @param string $card
     * @return string
     */
    public function getSuitFromString(string $card): string;

}
