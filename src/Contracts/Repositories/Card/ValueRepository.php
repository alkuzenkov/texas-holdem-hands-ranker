<?php

namespace Ranker\Contracts\Repositories\Card;

/**
 * Interface ValueRepository
 * @package Ranker\Contracts\Repositories\Card
 */
interface ValueRepository {

    /**
     * Gets the rank of passed value. Rank 0 is highest.
     * @param string $value
     * @return int
     */
    public function getRankOf(string $value): int;

    /**
     * Get all card values in order by rank
     * @return string[]
     */
    public function getAllRanked(): array;

}
