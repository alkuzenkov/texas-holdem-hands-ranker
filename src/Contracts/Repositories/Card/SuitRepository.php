<?php

namespace Ranker\Contracts\Repositories\Card;

/**
 * Interface SuitRepository
 * @package Ranker\Contracts\Repositories\Card
 */
interface SuitRepository {

    /**
     * Get all suits
     * @return string[]
     */
    public function getAll(): array;

}
