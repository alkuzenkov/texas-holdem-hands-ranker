<?php

namespace Ranker\Contracts\Repositories;

/**
 * Interface CombinationRepository
 * @package Ranker\Contracts\Repositories
 */
interface CombinationRepository {

    /**
     * Get all classes that implements combinations in order by rank. Rank 0 is highest
     * @return string[]
     */
    public function getAllRanked(): array;

}