<?php

namespace Ranker\Contracts\Controllers;

/**
 * Interface RankerController
 * @package Ranker\Contracts\Controllers
 */
interface RankerController {

    /**
     * @param array $hands_data
     */
    public function rank(array $hands_data): void;

}
