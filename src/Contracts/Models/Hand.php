<?php

namespace Ranker\Contracts\Models;

/**
 * Interface of Hand model
 * @package Ranker\Contracts\Models
 */
interface Hand {

    /**
     * @param int $id
     * @return mixed
     */
    public function setId(int $id): void;

    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @param Card $card
     */
    public function addCard(Card $card): void;

    /**
     * @return int
     */
    public function getCardsCount(): int;

    /**
     * @return Card[]
     */
    public function getCards(): array;

    /**
     * @return string
     */
    public function __toString();

}
