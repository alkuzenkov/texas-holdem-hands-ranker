<?php

namespace Ranker\Contracts\Models;

/**
 * Interface of Card model
 * @package Ranker\Contracts\Models
 */
interface Card {

    /**
     * @return string
     */
    public function getValue(): string;

    /**
     * @return string
     */
    public function getSuit(): string;

    /**
     * @param string $value
     */
    public function setValue(string $value): void;

    /**
     * @param string $suit
     */
    public function setSuit(string $suit): void;

    /**
     * @return string
     */
    public function __toString();

}
