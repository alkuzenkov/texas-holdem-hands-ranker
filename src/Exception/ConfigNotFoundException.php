<?php

namespace Ranker\Exception;

use RuntimeException;

class ConfigNotFoundException extends RuntimeException {

}