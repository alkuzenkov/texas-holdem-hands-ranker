<?php

namespace Ranker\Exception;

use UnexpectedValueException;

class BadInputException extends UnexpectedValueException {

}