<?php

namespace Ranker\Exception\Card;

use UnexpectedValueException;

class InvalidCardException extends UnexpectedValueException {

}