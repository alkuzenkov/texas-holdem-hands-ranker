<?php

namespace Ranker\Repositories\Card;

use Ranker\Contracts\Repositories\Card\SuitRepository as RepositoryContract;

/**
 * Class SuitRepository
 * @package Ranker\Repositories\Card
 */
class SuitRepository implements RepositoryContract {

    protected const SUITS = ['♦', '♣', '♥', '♠'];

    /**
     * @return string[]
     */
    public function getAll(): array {
        return static::SUITS;
    }

}
