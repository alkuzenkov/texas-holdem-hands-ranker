<?php

namespace Ranker\Repositories\Card;

use Ranker\Contracts\Repositories\Card\ValueRepository as RepositoryContract;
use Ranker\Exception\Card;

/**
 * Class ValueRepository
 * @package Ranker\Repositories\Card
 */
class ValueRepository implements RepositoryContract {

    protected const RANKED_VALUES = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];

    /**
     * @param string $value
     * @return int
     * @throws Card\ValueNotFoundException
     */
    public function getRankOf(string $value): int {
        $rank = array_search($value, $this->getAllRanked());
        if (!is_int($rank)) {
            throw new Card\ValueNotFoundException;
        }
        return $rank;
    }

    /**
     * @return string[]
     */
    public function getAllRanked(): array {
        return static::RANKED_VALUES;
    }

}
