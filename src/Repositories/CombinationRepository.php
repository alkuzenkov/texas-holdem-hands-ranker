<?php

namespace Ranker\Repositories;

use Ranker\Contracts\Repositories\CombinationRepository as RepositoryContract;
use Ranker\Services\Combinations\Flush;
use Ranker\Services\Combinations\FourOfAKind;
use Ranker\Services\Combinations\FullHouse;
use Ranker\Services\Combinations\Pair;
use Ranker\Services\Combinations\RoyalFlush;
use Ranker\Services\Combinations\Straight;
use Ranker\Services\Combinations\StraightFlush;
use Ranker\Services\Combinations\ThreeOfAKind;
use Ranker\Services\Combinations\TwoPair;

/**
 * Class CombinationRepository
 * @package Ranker\Repositories
 */
class CombinationRepository implements RepositoryContract {

    protected const RANKED_COMBINATIONS = [
        RoyalFlush::class,
        StraightFlush::class,
        FourOfAKind::class,
        FullHouse::class,
        Flush::class,
        Straight::class,
        ThreeOfAKind::class,
        TwoPair::class,
        Pair::class,
    ];

    /**
     * @return string[]
     */
    public function getAllRanked(): array {
        return static::RANKED_COMBINATIONS;
    }

}
