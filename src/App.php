<?php

namespace Ranker;

use DI\ContainerBuilder;
use Psr\Container\ContainerInterface;
use Ranker\Contracts\Strategies\InputStrategy;
use Ranker\Contracts\Controllers\RankerController;
use Ranker\Exception\ConfigNotFoundException;
use Ranker\Strategies\Input\FileStrategy;

/**
 * Main application class
 * @package Ranker
 */
class App {

    /** @var ContainerInterface */
    private $container;

    /** @var InputStrategy */
    private $input_strategy;

    /** @var self */
    private static $instance;

    /**
     * @return self
     */
    public static function i(): self {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    /**
     * App constructor.
     */
    private function __construct() {
        $this->buildContainer();
    }


    private function __clone() {
    }

    private function __wakeup() {
    }

    /**
     * Build and set DI container
     */
    private function buildContainer(): void {
        $builder = new ContainerBuilder;
        $config  = $this->getDiConfig();
        if ($config) {
            $builder->addDefinitions($config);
        }
        $this->setContainer($builder->build());
    }

    /**
     * Get dependency injection config
     * @return array
     */
    private function getDiConfig(): array {
        $config = require_once __DIR__ . '/../config/di.php';
        if (!is_array($config)) {
            throw new ConfigNotFoundException('Invalid dependency injection config');
        }
        return $config;
    }

    /**
     * @param ContainerInterface $container
     */
    private function setContainer(ContainerInterface $container): void {
        $this->container = $container;
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer(): ContainerInterface {
        return $this->container;
    }

    /**
     * Run application with input from a given file
     * @param string $filepath
     */
    public function runFromFile(string $filepath): void {
        $this->getContainer()->get(FileStrategy::class);
        $this->setInputStrategy($this->getContainer()->get(FileStrategy::class));
        $hands = $this->getInputStrategy()->parseData($filepath);
        $this->getContainer()->get(RankerController::class)->rank($hands);
    }

    /**
     * @param InputStrategy $strategy
     */
    private function setInputStrategy(InputStrategy $strategy): void {
        $this->input_strategy = $strategy;
    }

    /**
     * @return InputStrategy
     */
    private function getInputStrategy(): InputStrategy {
        return $this->input_strategy;
    }

}
