<?php

namespace Ranker\Services;

use Psr\Container\ContainerInterface;
use Ranker\Contracts\Services\CombinationChecker;
use Ranker\Contracts\Models\Hand;
use Ranker\Contracts\Repositories\CombinationRepository;
use Ranker\Contracts\Services\CardService;
use Ranker\Contracts\Services\HandService as ServiceContract;
use Ranker\Exception\Hand\CardsCountException;

/**
 * Class HandService
 * @package Ranker\Services
 */
class HandService implements ServiceContract {

    /** @var ContainerInterface */
    protected $container;

    /** @var CardService */
    protected $card_service;

    /** @var CombinationRepository */
    protected $combinations_repository;

    /**
     * HandService constructor.
     * @param ContainerInterface    $container
     * @param CardService           $card_service
     * @param CombinationRepository $combination_repository
     */
    public function __construct(
        ContainerInterface $container,
        CardService $card_service,
        CombinationRepository $combination_repository
    ) {
        $this->setContainer($container);
        $this->setCardService($card_service);
        $this->setCombinationsRepository($combination_repository);
    }

    /**
     * @param int   $id
     * @param array $cards
     * @return Hand
     */
    public function createHandFromArrayOfStrings(int $id, array $cards): Hand {
        $hand_class = $this->getContainer()->get(Hand::class);
        /** @var Hand $hand */
        $hand = new $hand_class;
        $hand->setId($id);
        foreach ($cards as $card_str) {
            $hand->addCard($this->getCardService()->createCardFromString($card_str));
        }
        if ($hand->getCardsCount() !== 5) {
            throw new CardsCountException('Count of cards in one hand should be equal to 5');
        }
        return $hand;
    }

    /**
     * @param array $hands
     * @return array
     */
    public function rank(array $hands): array {
        $combinations = $this->getCombinationsRepository()->getAllRanked();
        $lowest_rank  = count($combinations);
        $ranked       = [];
        foreach ($hands as $hand) {
            foreach ($combinations as $i => $class) {
                /** @var CombinationChecker $combination */
                $combination = $this->getContainer()->get($class);
                if ($combination->matches($hand)) {
                    $ranked[$i][] = $hand;
                    continue 2;
                }
            }
            $ranked[$lowest_rank][] = $hand;
        }
        ksort($ranked);
        $result = [];
        foreach ($ranked as $equal_hands) {
            $result = array_merge($result, $equal_hands);
        }
        return $result;
    }

    /**
     * @return ContainerInterface
     */
    protected function getContainer(): ContainerInterface {
        return $this->container;
    }

    /**
     * @param ContainerInterface $container
     */
    protected function setContainer(ContainerInterface $container): void {
        $this->container = $container;
    }

    /**
     * @return CardService
     */
    protected function getCardService(): CardService {
        return $this->card_service;
    }

    /**
     * @param CardService $card_service
     */
    protected function setCardService(CardService $card_service): void {
        $this->card_service = $card_service;
    }

    /**
     * @return CombinationRepository
     */
    protected function getCombinationsRepository(): CombinationRepository {
        return $this->combinations_repository;
    }

    /**
     * @param CombinationRepository $combinations_repository
     */
    protected function setCombinationsRepository(CombinationRepository $combinations_repository): void {
        $this->combinations_repository = $combinations_repository;
    }

}
