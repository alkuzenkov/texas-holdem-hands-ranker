<?php

namespace Ranker\Services\Combinations;

use Ranker\Contracts\Services\CombinationChecker;
use Ranker\Contracts\Models\Hand;
use Ranker\Contracts\Repositories\Card\ValueRepository;
use Ranker\Exception\Card\NotEnoughCardValuesException;

/**
 * Class RoyalFlush
 * @package Ranker\Services\Combinations
 */
class RoyalFlush implements CombinationChecker {

    /** @var ValueRepository */
    protected $value_repository;

    /**
     * RoyalFlush constructor.
     * @param ValueRepository $value_repository
     */
    public function __construct(ValueRepository $value_repository) {
        $this->setValueRepository($value_repository);
    }

    /**
     * Check for combination like A♦ K♦ Q♦ J♦ 10♦
     * @param Hand $hand
     * @return bool
     * @throws NotEnoughCardValuesException
     */
    public function matches(Hand $hand): bool {
        $cards           = $hand->getCards();
        $all_values      = $this->getValueRepository()->getAllRanked();
        $values_to_match = array_slice($all_values, -5);
        if (count($values_to_match) !== 5) {
            throw new NotEnoughCardValuesException;
        }
        $suit  = $cards[0]->getSuit();
        foreach ($cards as $card) {
            if ($card->getSuit() !== $suit) {
                break;
            }
            $i = array_search($card->getValue(), $values_to_match);
            if ($i === false) {
                break;
            }
            unset($values_to_match[$i]);
        }
        return count($values_to_match) === 0;
    }

    /**
     * @return ValueRepository
     */
    protected function getValueRepository(): ValueRepository {
        return $this->value_repository;
    }

    /**
     * @param ValueRepository $card_service
     */
    protected function setValueRepository(ValueRepository $card_service): void {
        $this->value_repository = $card_service;
    }

}
