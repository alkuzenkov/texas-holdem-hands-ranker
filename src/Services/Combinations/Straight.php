<?php

namespace Ranker\Services\Combinations;

use Ranker\Contracts\Services\CombinationChecker;
use Ranker\Contracts\Models\Hand;
use Ranker\Contracts\Services\CardService;

/**
 * Class Straight
 * @package Ranker\Services\Combinations
 */
class Straight implements CombinationChecker {

    /** @var CardService */
    protected $card_service;

    /**
     * Straight constructor.
     * @param CardService $card_service
     */
    public function __construct(CardService $card_service) {
        $this->setCardService($card_service);
    }

    /**
     * Check for combination like 9♣ 8♦ 7♠ 6♦ 5♥
     * @param Hand $hand
     * @return bool
     */
    public function matches(Hand $hand): bool {
        $cards       = $this->getCardService()->sort($hand->getCards());
        $cards_count = count($cards);
        $same_suit   = true;
        for ($i = 1; $i < $cards_count; $i++) {
            $curr_card = $cards[$i];
            $prev_card = $cards[$i - 1];
            if (!$this->getCardService()->isCloseValues($cards[$i - 1], $cards[$i])) {
                return false;
            }
            if ($same_suit) {
                $same_suit = $prev_card->getSuit() === $curr_card->getSuit();
            }
        }
        return !$same_suit;
    }

    /**
     * @return CardService
     */
    protected function getCardService(): CardService {
        return $this->card_service;
    }

    /**
     * @param CardService $card_service
     */
    protected function setCardService(CardService $card_service): void {
        $this->card_service = $card_service;
    }

}
