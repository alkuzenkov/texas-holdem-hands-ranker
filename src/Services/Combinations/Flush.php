<?php

namespace Ranker\Services\Combinations;

use Ranker\Contracts\Services\CombinationChecker;
use Ranker\Contracts\Models\Hand;
use Ranker\Contracts\Services\CardService;

/**
 * Class Flush
 * @package Ranker\Services\Combinations
 */
class Flush implements CombinationChecker {

    /** @var CardService */
    protected $card_service;

    /**
     * Flush constructor.
     * @param CardService $card_service
     */
    public function __construct(CardService $card_service) {
        $this->setCardService($card_service);
    }

    /**
     * Check for combination like 4♠ J♠ 8♠ 2♠ 9♠
     * @param Hand $hand
     * @return bool
     */
    public function matches(Hand $hand): bool {
        $cards       = $this->getCardService()->sort($hand->getCards());
        $cards_count = count($cards);
        $has_sequence = true;
        for ($i = 1; $i < $cards_count; $i++) {
            $curr_card = $cards[$i];
            $prev_card = $cards[$i - 1];
            if ($curr_card->getSuit() !== $prev_card->getSuit()) {
                return false;
            }
            if ($has_sequence) {
                $has_sequence = $this->getCardService()->isCloseValues($prev_card, $curr_card);
            }
        }
        return !$has_sequence;
    }

    /**
     * @return CardService
     */
    protected function getCardService(): CardService {
        return $this->card_service;
    }

    /**
     * @param CardService $card_service
     */
    protected function setCardService(CardService $card_service): void {
        $this->card_service = $card_service;
    }

}
