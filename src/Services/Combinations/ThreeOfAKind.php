<?php

namespace Ranker\Services\Combinations;

use Ranker\Contracts\Services\CombinationChecker;
use Ranker\Contracts\Models\Hand;

/**
 * Class ThreeOfAKind
 * @package Ranker\Services\Combinations
 */
class ThreeOfAKind implements CombinationChecker {

    /**
     * Check for combination like 7♣ 7♦ 7♠ K♣ 3♦
     * @param Hand $hand
     * @return bool
     */
    public function matches(Hand $hand): bool {
        $cards = $hand->getCards();
        foreach ($cards as $base_card) {
            $matches = 0;
            foreach ($cards as $compare_card) {
                if ($base_card->getValue() === $compare_card->getValue()) {
                    ++$matches;
                }
            }
            if ($matches === 3) {
                return true;
            }
        }
        return false;
    }

}
