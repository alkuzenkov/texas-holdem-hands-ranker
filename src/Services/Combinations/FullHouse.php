<?php

namespace Ranker\Services\Combinations;

use Ranker\Contracts\Services\CombinationChecker;
use Ranker\Contracts\Models\Hand;

/**
 * Class FullHouse
 * @package Ranker\Services\Combinations
 */
class FullHouse implements CombinationChecker {

    /**
     * Check for combination like 10♥ 10♦ 10♠ 9♣ 9♦
     * @param Hand $hand
     * @return bool
     */
    public function matches(Hand $hand): bool {
        $first_value        = null;
        $second_value       = null;
        $first_value_count  = 0;
        $second_value_count = 0;
        foreach ($hand->getCards() as $card) {
            if ($first_value === null) {
                $first_value = $card->getValue();
            } elseif ($first_value !== $card->getValue() && $second_value === null) {
                $second_value = $card->getValue();
            }
            if ($first_value === $card->getValue()) {
                ++$first_value_count;
            } elseif ($second_value === $card->getValue()) {
                ++$second_value_count;
            }
        }
        return ($first_value_count === 3 && $second_value_count === 2)
            || ($first_value_count === 2 && $second_value_count === 3);
    }

}
