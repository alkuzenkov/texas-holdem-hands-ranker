<?php

namespace Ranker\Services\Combinations;

use Ranker\Contracts\Services\CombinationChecker;
use Ranker\Contracts\Models\Hand;
use Ranker\Contracts\Services\CardService;

/**
 * Class StraightFlush
 * @package Ranker\Services\Combinations
 */
class StraightFlush implements CombinationChecker {

    /** @var CardService */
    protected $card_service;

    /**
     * StraightFlush constructor.
     * @param CardService $card_service
     */
    public function __construct(CardService $card_service) {
        $this->setCardService($card_service);
    }

    /**
     * Check for combination like 8♣ 7♣ 6♣ 5♣ 4♣
     * @param Hand $hand
     * @return bool
     */
    public function matches(Hand $hand): bool {
        $cards       = $this->getCardService()->sort($hand->getCards());
        $cards_count = count($cards);
        for ($i = 1; $i < $cards_count; $i++) {
            $curr_card = $cards[$i];
            $prev_card = $cards[$i - 1];
            if (
                $curr_card->getSuit() !== $prev_card->getSuit()
                || !$this->getCardService()->isCloseValues($curr_card, $prev_card)
            ) {
                return false;
            }
        }
        return true;
    }

    /**
     * @return CardService
     */
    protected function getCardService(): CardService {
        return $this->card_service;
    }

    /**
     * @param CardService $card_service
     */
    protected function setCardService(CardService $card_service): void {
        $this->card_service = $card_service;
    }

}
