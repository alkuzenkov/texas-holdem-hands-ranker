<?php

namespace Ranker\Services\Combinations;

use Ranker\Contracts\Services\CombinationChecker;
use Ranker\Contracts\Models\Hand;

/**
 * Class TwoPair
 * @package Ranker\Services\Combinations
 */
class TwoPair implements CombinationChecker {

    /**
     * Check for combination like 4♣ 4♠ 3♣ 3♦ Q♣
     * @param Hand $hand
     * @return bool
     */
    public function matches(Hand $hand): bool {
        $cards            = $hand->getCards();
        $first_pair_value = null;
        foreach ($cards as $base_card) {
            $matches = 0;
            foreach ($cards as $compare_card) {
                if ($base_card->getValue() === $compare_card->getValue() && $base_card->getValue() !== $first_pair_value) {
                    ++$matches;
                }
            }
            if ($matches !== 2) {
                continue;
            }
            if ($first_pair_value !== null) {
                return true;
            }
            $first_pair_value = $base_card->getValue();
        }
        return false;
    }

}
