<?php

namespace Ranker\Services\Combinations;

use Ranker\Contracts\Services\CombinationChecker;
use Ranker\Contracts\Models\Hand;

/**
 * Class FourOfAKind
 * @package Ranker\Services\Combinations
 */
class FourOfAKind implements CombinationChecker {

    /**
     * Check for combinations like J♥ J♦ J♠ J♣ 7♦
     * @param Hand $hand
     * @return bool
     */
    public function matches(Hand $hand): bool {
        $cards = $hand->getCards();
        foreach ($cards as $base_card) {
            $matches = 0;
            foreach ($cards as $compare_card) {
                if ($base_card->getValue() === $compare_card->getValue()) {
                    ++$matches;
                }
            }
            if ($matches === 4) {
                return true;
            }
        }
        return false;
    }

}