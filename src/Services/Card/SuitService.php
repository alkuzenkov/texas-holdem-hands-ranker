<?php

namespace Ranker\Services\Card;

use Ranker\Contracts\Repositories\Card\SuitRepository;
use Ranker\Contracts\Services\Card\SuitService as ServiceContract;
use Ranker\Exception\Card\InvalidCardException;

class SuitService implements ServiceContract {

    /** @var SuitRepository */
    protected $repository;

    /**
     * SuitService constructor.
     * @param SuitRepository $repository
     */
    public function __construct(SuitRepository $repository) {
        $this->setRepository($repository);
    }

    /**
     * @param string $card
     * @return string
     */
    public function getSuitFromString(string $card): string {
        $values = implode('|', $this->getRepository()->getAll());
        preg_match("/(?:{$values})$/", $card, $matches);
        if (!$matches) {
            throw new InvalidCardException('Suit not extracted from string');
        }
        return $matches[0];
    }

    /**
     * @return SuitRepository
     */
    protected function getRepository(): SuitRepository {
        return $this->repository;
    }

    /**
     * @param SuitRepository $repository
     */
    protected function setRepository(SuitRepository $repository): void {
        $this->repository = $repository;
    }

}
