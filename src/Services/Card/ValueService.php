<?php

namespace Ranker\Services\Card;

use Ranker\Contracts\Repositories\Card\ValueRepository;
use Ranker\Contracts\Services\Card\ValueService as ServiceContract;
use Ranker\Exception\Card\InvalidCardException;

/**
 * Class ValueService
 * @package Ranker\Services\Card
 */
class ValueService implements ServiceContract {

    /** @var ValueRepository */
    protected $repository;

    /**
     * ValueService constructor.
     * @param ValueRepository $repository
     */
    public function __construct(ValueRepository $repository) {
        $this->setRepository($repository);
    }

    /**
     * @param string $card
     * @return string
     */
    public function getValueFromString(string $card): string {
        $values = implode('|', $this->getRepository()->getAllRanked());
        preg_match("/^(?:{$values})/", $card, $matches);
        if (!$matches) {
            throw new InvalidCardException('Value not extracted from string');
        }
        return $matches[0];
    }

    /**
     * @return ValueRepository
     */
    protected function getRepository(): ValueRepository {
        return $this->repository;
    }

    /**
     * @param ValueRepository $repository
     */
    protected function setRepository(ValueRepository $repository): void {
        $this->repository = $repository;
    }

}
