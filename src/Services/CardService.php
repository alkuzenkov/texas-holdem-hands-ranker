<?php

namespace Ranker\Services;

use Psr\Container\ContainerInterface;
use Ranker\Contracts\Models\Card;
use Ranker\Contracts\Repositories\Card\ValueRepository;
use Ranker\Contracts\Services\Card\SuitService;
use Ranker\Contracts\Services\Card\ValueService;
use Ranker\Contracts\Services\CardService as ServiceContract;
use Ranker\Exception\Card\InvalidCardException;

/**
 * Class CardService
 * @package Ranker\Services
 */
class CardService implements ServiceContract {

    /** @var ValueService */
    protected $value_service;

    /** @var SuitService */
    protected $suit_service;

    /** @var ContainerInterface */
    protected $container;

    /** @var ValueRepository */
    protected $value_repository;

    /**
     * CardService constructor.
     * @param ValueService       $value_service
     * @param SuitService        $suit_service
     * @param ContainerInterface $container
     * @param ValueRepository    $value_repository
     */
    public function __construct(
        ValueService $value_service,
        SuitService $suit_service,
        ContainerInterface $container,
        ValueRepository $value_repository
    ) {
        $this->setValueService($value_service);
        $this->setSuitService($suit_service);
        $this->setContainer($container);
        $this->setValueRepository($value_repository);
    }

    /**
     * @param string $card_str
     * @return Card
     */
    public function createCardFromString(string $card_str): Card {
        if (!strlen($card_str)) {
            throw new InvalidCardException('Cannot create card from empty string');
        }
        $card_class = $this->getContainer()->get(Card::class);
        /** @var Card $card */
        $card = new $card_class;
        $card->setValue($this->getValueService()->getValueFromString($card_str));
        $card->setSuit($this->getSuitService()->getSuitFromString($card_str));
        return $card;
    }

    /**
     * @param Card[] $cards
     * @return Card[]
     */
    public function sort(array $cards): array {
        $result = [];
        while ($cards) {
            $pick_at = null;
            foreach ($cards as $i => $card) {
                if ($pick_at === null || $this->compare($cards[$pick_at], $card) === 1) {
                    $pick_at = $i;
                }
            }
            $result[] = $cards[$pick_at];
            unset($cards[$pick_at]);
        }
        return $result;
    }

    /**
     * @param Card $a
     * @param Card $b
     * @return int
     */
    public function compare(Card $a, Card $b): int {
        $repository = $this->getValueRepository();
        return $repository->getRankOf($a->getValue()) <=> $repository->getRankOf($b->getValue());
    }

    /**
     * @param Card $a
     * @param Card $b
     * @return bool
     */
    public function isCloseValues(Card $a, Card $b): bool {
        $repository = $this->getValueRepository();
        $rank_a     = $repository->getRankOf($a->getValue());
        $rank_b     = $repository->getRankOf($b->getValue());
        return $rank_a + 1 === $rank_b || $rank_b + 1 === $rank_a;
    }

    /**
     * @param ValueService $value_service
     */
    protected function setValueService(ValueService $value_service): void {
        $this->value_service = $value_service;
    }

    /**
     * @return ValueService
     */
    protected function getValueService(): ValueService {
        return $this->value_service;
    }

    /**
     * @return SuitService
     */
    protected function getSuitService(): SuitService {
        return $this->suit_service;
    }

    /**
     * @param SuitService $suit_service
     */
    protected function setSuitService(SuitService $suit_service): void {
        $this->suit_service = $suit_service;
    }

    /**
     * @return ContainerInterface
     */
    protected function getContainer(): ContainerInterface {
        return $this->container;
    }

    /**
     * @param ContainerInterface $container
     */
    protected function setContainer(ContainerInterface $container): void {
        $this->container = $container;
    }

    /**
     * @return ValueRepository
     */
    protected function getValueRepository(): ValueRepository {
        return $this->value_repository;
    }

    /**
     * @param ValueRepository $value_repository
     */
    protected function setValueRepository(ValueRepository $value_repository): void {
        $this->value_repository = $value_repository;
    }

}
