<?php

namespace Ranker\Controllers;

use Ranker\Contracts\Controllers\RankerController as ControllerContract;
use Ranker\Contracts\Services\HandService;

/**
 * Class RankerController
 * @package Ranker\Controllers
 */
class RankerController implements ControllerContract {

    /** @var HandService */
    protected $hand_service;

    /**
     * RankerController constructor.
     * @param HandService $hand_service
     */
    public function __construct(HandService $hand_service) {
        $this->setHandService($hand_service);
    }

    /**
     * @param array $hands_data
     */
    public function rank(array $hands_data): void {
        $hands = [];
        foreach ($hands_data as $i => $hand_arr) {
            $hands[] = $this->getHandService()->createHandFromArrayOfStrings($i, $hand_arr);
        }
        foreach ($this->getHandService()->rank($hands) as $hand) {
            echo $hand . PHP_EOL;
        }
    }

    /**
     * @return HandService
     */
    protected function getHandService(): HandService {
        return $this->hand_service;
    }

    /**
     * @param HandService $hand_service
     */
    protected function setHandService(HandService $hand_service): void {
        $this->hand_service = $hand_service;
    }

}
